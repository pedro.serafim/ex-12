import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int codigo;
        Imprime tela = new Imprime();
        Scanner entrada = new Scanner (System.in);
        tela.imprime("Verificador de Código\n");
        tela.imprime("Digite o Código: ");
        CapturaScanner scanner = new CapturaScanner();
        codigo = scanner.capturaScanner(entrada);


        switch (codigo) {
            case 1:
                tela.imprime("um");
                break;
            case 2:
                tela.imprime("dois");
                break;
            case 3:
                tela.imprime("três");
                break;
            default:
                tela.imprime("Código inválido");
        }

    }


}



